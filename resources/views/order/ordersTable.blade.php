@extends('layouts.app')
@section('pageTitle', 'MysteryBox | سفارش ها')
@section('content')
	<div class="white row">
		<table class="highlight centered" style="table-layout: fixed">
			<thead>
				<tr>
					<th>آیدی سفارش</th>
					<th>نام مشتری</th>
					<th>شماره مشتری</th>
					<th>نام گیرنده</th>
					<th>شماره گیرنده</th>
					<th>تاریخ دریافت</th>
					<th>هدیه میستری باکس</th>
					<th>هدیه سفارشی</th>
					<th>آدرس خونه طرف</th>
					<th>شماره کارت طرف</th>
				</tr>
			</thead>
			<tbody>
			@foreach($orders as $order)
				<tr>
					<td>{{$order->id}}</td>
					<td>{{$order->sender_name." ".$order->sender_last_name}}</td>
					<td>{{$order->sender_mobile_number}}</td>
					<td>{{$order->receiver_name." ".$order->receiver_last_name}}</td>
					<td>{{$order->receiver_mobile_number}}</td>
					<td>{{$order->gift_time}}</td>
					<td>{{$order->gifts}}</td>
					<td>
						<p>
							{{$order->custom_gift}}
						</p>
					</td>
					<td>
						<p>
							{{$order->receiver_address}}
						</p>
					</td>
					<td>
						@if(!empty($order->user))
						{{$order->user->card_number}}
							@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection
@extends('layouts.app')
@section('title', 'اتمام خرید')
@section('content')
	<section class="Introduction-game">
		<img class="main-logo" src="/images/mainLogo.png" alt="">
		<div class="container center-align free-text">
			<h1 class="huge-font cyan-text text-accent-3">
				<div class="huge-font" style="line-height:2;font-weight: bold; font-size: 2em;">سفارش شما با موفقیت ثبت شد! </div><br><br>
				<div class="huge-font">شما یک رفیق واقعی هستید! :)</div>
			</h1>
			<div class="row">
			<p class="huge-font cyan-text text-accent-3 col l8 m8 s12 offset-l2 offset-m2 checkout-text">
				از اینکه این هدیه رو برای دوستت ارسال کردی خیلی خوشحالیم و مطمئنیم که دوستت هم خیلی خوشحال میشه. <br>
				اگه نیاز بود برای هماهنگی سفارش حتما باهاتون تماس می‌گیریم. <br>
				می‌خوای این هدیه رو برای یکی دو نفر دیگه از دوستای صمیمی یا دورت هم  ارسال کنی ?!
			</p>
			</div>
		</div>
	</section>
	<div class="center-align">
		<br><br><br>
		<a href="/order" class="huge-font btn btn-large cyan accent-3 Dark-text">ارسال برای دوستان بیشتر</a><br><br><br>
		<a href="/plans" class="cyan-text text-accent-3">متشکرم نیازی نیست</a>
		<br><br><br>
	</div>
@endsection
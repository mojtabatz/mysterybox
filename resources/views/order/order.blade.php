@extends('layouts.app')
@section('pageTitle', 'MysteryBox | ثبت هدیه جدید')
@section('metaData')
	<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.min.js"></script>
	<script src="/js/games/order.js"></script>
@endsection
@section('content')
	@if ($errors->any())
		<ul id="Mahdi">
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	@endif
<section class="Order">
	<div class="intro row">
		<figure>
			<img class="fullWidth" src="/images/mainLogo.png" alt="">
		</figure>
		<div  class="white-text col l12 ">
			<h1 class="center-align">یه هدیه خاص برای یه دوست خاص</h1>
			<a id="ScrollToDownButton" href="#FormContainer" class="pulse btn-large cyan-text  text-accent-3 grey darken-3">خوشحالش کن&nbsp; :)</a>
		</div>

	</div>
	<div id="FormContainer" class="container">
		<form class="row" method="POST" id="OrderForm" enctype="multipart/form-data" action="{{ url('/order') }}">
			{{ csrf_field() }}
			<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3">
				<input id="customerName" type="text" class="validate white-text" name="sender_name" required>
				<label for="customerName" class="cyan-text text-accent-3">اسم خودت</label>
				<span class="helper-text" data-error="این فیلد رو حتما پر کن" data-success="OK"></span>	
			</div>
			<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3">
				<input id="customerLastName" type="text" class="validate white-text" name="sender_last_name" required>
				<label for="customerLastName" class="cyan-text text-accent-3">اسم خانوادگی خودت</label>
				<span class="helper-text" data-error="این فیلد رو حتما پر کن" data-success="OK"></span>	
			</div>
			<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3">
				<input id="customerPhoneNumber" type="text" class="validate white-text" name="sender_mobile_number" required>
				<label for="customerPhoneNumber" class="cyan-text text-accent-3">شماره همراه خودت</label>
				<span class="helper-text" data-error="این فیلد رو حتما پر کن" data-success="OK"></span>	
			</div>
			<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3">
				<input id="customerFriendName" type="text" class="validate white-text" name="receiver_name" required>
				<label for="customerFriendName" class="cyan-text text-accent-3"> اسم دوستت</label>
				<span class="helper-text" data-error="این فیلد رو حتما پر کن" data-success="OK"></span>	
			</div>
			<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3">
				<input id="customerFriendLastName" type="text" class="validate white-text" name="receiver_last_name" required>
				<label for="customerFriendLastName" class="cyan-text text-accent-3"> اسم خانوادگی دوستت</label>
				<span class="helper-text" data-error="این فیلد رو حتما پر کن" data-success="OK"></span>	
			</div>
			<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3">
				<input id="customerFriendPhone" type="text" class="validate white-text" name="receiver_mobile_number" required>
				<label for="customerFriendPhone" class="cyan-text text-accent-3">شماره همراه دوستت</label>
				<span class="helper-text" data-error="این فیلد رو حتما پر کن" data-success="OK"></span>	
			</div>
			<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3">
				<div class="cyan-text text-accent-3 big-font right-align">چه زمانی هدیه برای دوستت ارسال بشه</div><br><br>
				<div class="clear-fix"></div>
				<div class="combobox">
					<select name="month">
						<option value="" disabled selected> انتخاب ماه</option>
						<option value="1">فروردین</option>
						<option value="2">اردیبهشت</option>
						<option value="3">خرداد</option>
						<option value="4">تیر</option>
						<option value="5">مرداد</option>
						<option value="6">شهرویور</option>
						<option value="7">مهر</option>
						<option value="8">آبان</option>
						<option value="9">آذر</option>
						<option value="10">دی</option>
						<option value="11">بهمن</option>
						<option value="12">اسفند</option>
					</select>
				</div>
				<div class="combobox">
					<select name="day">
						<option value="" disabled selected>چه روزی از ماه ؟</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
					</select>
				</div>
				<div class="combobox">
					<select name="hour">
						<option value="" disabled selected>تو چه ساعتی  ؟</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
					</select>
				</div>
			</div>
			<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3">
				<h2 class="cyan-text text-accent-3 big-font">چه متنی دوست داری به دوستت بفرستی ؟</h2><br>
				<p>
					<label>
						<input  data-type="GiftTemplate" id="TemplateOne" class="with-gap" name="GiftTextTemplate" type="radio" />
						<span class="cyan-text text-accent-3">متن دوستانه ۱</span>
					</label>
				</p>
				<p>
					<label>
						<input data-type="GiftTemplate" id="TemplateTwo"  class="with-gap" name="GiftTextTemplate" type="radio" />
						<span class="cyan-text text-accent-3">متن دوستانه ۲</span>
					</label>
				</p>
				<p>
					<label>
						<input data-type="GiftTemplate" id="TemplateFour"  class="with-gap" name="GiftTextTemplate" type="radio" />
						<span class="cyan-text text-accent-3">متن دوستانه ۳</span>
					</label>
				</p>	
				<p>
					<label>
						<input data-type="GiftTemplate" id="TemplateThree"  class="with-gap" name="GiftTextTemplate" type="radio" />
						<span class="cyan-text text-accent-3">متن دوستانه صمیمی ۱</span>
					</label>
				</p>	
				<p>
					<label>
						<input data-type="GiftTemplate" id="TemplateFive"  class="with-gap" name="GiftTextTemplate" type="radio" />
						<span class="cyan-text text-accent-3">متن دوستانه صمیمی ۲</span>
					</label>
				</p>	
				<p>
					<label>
						<input data-type="GiftTemplate" id="TemplateSix"  class="with-gap" name="GiftTextTemplate" type="radio" />
						<span class="cyan-text text-accent-3">متن همسرانه صمیمی خودمونی</span>
					</label>
				</p>	
				<p>
					<label>
						<input data-type="GiftTemplate" id="TemplateSeven"  class="with-gap" name="GiftTextTemplate" type="radio" />
						<span class="cyan-text text-accent-3">متن همسرانه</span>
					</label>
				</p>	
				<p>
					<label>
						<input data-type="GiftTemplate" id="TemplateEight"  class="with-gap" name="GiftTextTemplate" type="radio" />
						<span class="cyan-text text-accent-3">متن تولد دوستانه</span>
					</label>
				</p>	
				<p>
					<label>
						<input data-type="GiftTemplate" id="TemplateNine"  class="with-gap" name="GiftTextTemplate" type="radio" />
						<span class="cyan-text text-accent-3">متن تولد صمیمی خودمونی</span>
					</label>
				</p>	
				<p>
					<label>
						<input data-type="GiftTemplate" id="TemplateTen"  class="with-gap" name="GiftTextTemplate" type="radio" />
						<span class="cyan-text text-accent-3">متن تولد رسمی</span>
					</label>
				</p>	
				<p>
					<label>
						<input data-type="GiftTemplate" id="TemplateFree"  class="with-gap" name="GiftTextTemplate" type="radio" />
						<span class="cyan-text text-accent-3">متن دلخواه خودت</span>
					</label>
				</p>	
			</div>
			<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3">
				<textarea id="GiftText" name="final_text" class="materialize-textarea white-text"></textarea>
				<label for="GiftText" class="cyan-text text-accent-3">پیغام مورد نظرت برای دوستت </label>
				<span class="helper-text" data-error="این فیلد رو حتما پر کن" data-success="OK"></span>
			</div>
			<div class="file-field input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3">
				<div class="btn btn-large cyan-text  text-accent-3 grey darken-3 right">
					<span>عکس دوستتو آپلود کن</span>
					<input type="file" name="file" id="file" accept="image/*" single required>
				</div>
				<div class="file-path-wrapper">
					<input class="file-path validate white-text" type="text">
				</div>
				<div class="left">
					<label for="file" style="cursor: pointer;"><img src="/images/uploadImage.jpg" alt=""></label>
				</div>
			</div>
			<div class="file-field input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3">
				<div class="btn btn-large cyan-text  text-accent-3 grey darken-3 right">
					<span>یه آهنگ برای دوستت بفرست</span>
					<input type="file" name="music" id="music" accept="audio/*" single required>
				</div>
				<div class="file-path-wrapper">
					<input class="file-path validate white-text" type="text">
				</div>
				<div class="left">
					<label for="music" style="cursor: pointer;"><img src="/images/uploadMusic.png" alt=""></label>
				</div>
			</div>
			<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3">
				<h2 class="cyan-text text-accent-3 big-font">هدیه های پیشنهادی برای دوستت (اختیاری)</h2><br>
				<p>
					<label>
						<input name="gifts[]" value="kitkat" data-price="3000_kitkat" data-type="OptionalGift" type="checkbox" />
						<span class="cyan-text text-accent-3">کیت کت چانکی</span>
						<img src="/images/kitkat.jpg" alt="">
						<em class="cyan-text text-accent-3">3000 تومان</em>
					</label>
				</p>
				<p>
					<label>
						<input name="gifts[]" value="nutella" data-price="14000_nutella" data-type="OptionalGift" type="checkbox" />
						<span class="cyan-text text-accent-3">نوتلا 180 گرمی</span>
						<img src="/images/nutella.jpg" alt="">
						<em class="cyan-text text-accent-3">14,000 تومان</em>
					</label>
				</p>
				<p>
					<label>
						<input name="gifts[]" value="pastil" data-price="10000_pastil" data-type="OptionalGift" type="checkbox" />
						<span class="cyan-text text-accent-3">پاستیل هاریبو استارمیکس 160 گرمی</span>
						<img src="/images/pastil.jpg" alt="">
						<em class="cyan-text text-accent-3">10,000 تومان</em>
					</label>
				</p>
				<p>
					<label>
						<input name="gifts[]" value="rose" data-price="10000_flower" data-type="OptionalGift" type="checkbox" />
						<span class="cyan-text text-accent-3">یک شاخه گل رز سرخ هلندی</span>
						<img src="/images/flower.jpg" alt="">
						<em class="cyan-text text-accent-3">10,000 تومان</em>
					</label>
				</p>
				<p>
					<label>
						<input name="gifts[]" value="gift50" data-price="50000_giftCard" data-type="OptionalGift" type="checkbox" />
						<span class="cyan-text text-accent-3">کارت هدیه ۵۰ هزار تومنی</span>
						<img src="/images/giftCard.jpg" alt="">
						<em class="cyan-text text-accent-3">50,000 تومان</em>
					</label>
				</p>
				<p>
					<label>
						<input name="gifts[]" value="gift100" data-price="100000_giftCard" data-type="OptionalGift" type="checkbox" />
						<span class="cyan-text text-accent-3">کارت هدیه ۱۰۰ هزار تومنی</span>
						<img src="/images/giftCard.jpg" alt="">
						<em class="cyan-text text-accent-3">100,000 تومان</em>
					</label>
				</p>
				<p>
					<label>
						<input name="gifts[]" value="gift200" data-price="200000_giftCard" data-type="OptionalGift" type="checkbox" />
						<span class="cyan-text text-accent-3">کارت هدیه ۲۰۰ هزار تومنی</span>
						<img src="/images/giftCard.jpg" alt="">
						<em class="cyan-text text-accent-3">200,000 تومان</em>
					</label>
				</p>
			</div>
			<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3">
				<input id="customGift" type="text" class="validate white-text" name="custom_gift">
				<label for="customGift" class="cyan-text text-accent-3">هدیه سفارشی</label>
				<span class="helper-text cyan-text text-accent-3">
					هر هدیه ای که بخوای ما برای دوستت تهیه می کنیم. قبل از خرید هزینه ش رو باهات هماهنگ میکنیم که همه چی اوکی باشه
				</span>	
			</div>
			<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3">
				<input id="UserAddress" type="text" name="receiver_address" class="validate white-text">
				<label for="UserAddress" class="cyan-text text-accent-3">آدرس خونه دوستت</label>
				<span class="helper-text cyan-text text-accent-3"> اگه هیچ هدیه فیزیکی برای دوستت انتخاب نکردی نیازی به وارد کردن آدرس نیست</span>
			</div>
			<div class="col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3">
				<div class="cyan-text text-accent-3 big-font overflow-hidden factor">
					<span class="right">هزینه هدایا :‌ </span>
					<span class="left">
						<span id="DiscountGifts">رایگان</span>
					</span> 
				</div>
				<div class="cyan-text text-accent-3 big-font overflow-hidden factor">
					<span class="right">هزینه ارسال :‌ </span>
					<span class="left">
						<span id="DiscountDelivery">رایگان</span>
					</span> 
				</div>
				<div class="cyan-text text-accent-3 big-font overflow-hidden factor">
					<span class="right">هزینه خدمات میستری باکس :‌ </span>
					<span class="left">
						<span id="DiscountFee">رایگان</span>
					</span> 
				</div>
				<div class="cyan-text text-accent-3 big-font overflow-hidden factor">
					<span class="right">هزینه کل :‌ </span>
					<span class="left">
						<span id="DiscountTotalPrice">رایگان</span>
					</span> 
				</div>
			</div>
			<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3">
				<button type="submit" id="OrderSubmitButton" class="fullWidth btn  cyan darken-2 white-text">
					ثبت
				</button>
				<div class="displayNone" id="Preloader">
					<div class="white-text huge-font center-align">
						لطفا صبر کنید. برنامه در حال آپلود اطلاعات شما است ...
					</div>
					<div class="progress">
						<div class="indeterminate"></div>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>
@endsection
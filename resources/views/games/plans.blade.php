@extends('layouts.app')
@section('title', 'پایان بازی')
@section('content')
<section class="Plans directionRtl">
		<img class="MainLogo" src="/images/mainLogo.png" alt="">
		<div class="container center-align">
			<h1 class="main-title center-align">
				<div class="cyan-text text-accent-3">Our Boxes</div>
				<div class="white-text">بسته های ما</div>
			</h1>
		</div>
		<div class="container Plans-items">
			<div class="row">
				<div class="col l4  s12 m5 right offset-l4 offset-m2">
					<figure class="logo"><img class="fullWidth" src="/images/gamebox.png" alt=""></figure>
					<p class="white-text bif-font justify-align ">کلی بازی های باحال و معمایی با کلی جوایز نقدی. هر هفته نفرات اول تا دهم هر بازی جایزه های نقدی میگیرند. فقط کافیه تلاشتو بکنی تا جز بهترین ها باشی و از بازی کردنت هم لذت ببری و هم  جایزه ببری!</p>
					<div class="price">
						<span>Box Price: </span>
						<span>Free !</span>
					</div>
					<div class="center-align white-text dark-button Dark-color">به زودی</div>
				</div>
				<div class="col l4 m5 s12 right">
					<figure class="logo"><img class="fullWidth" src="/images/giftbox.png" alt=""></figure>
					<p class="white-text bif-font justify-align ">یه مدل کادوی جدید و عجیب غریب برای هدیه دادن. دوستاتون رو  سورپرایز کنید و به مناسبت های مختلف با ارسال کادوهای دلخواهتون کلی خوشحالشون کنید. دوستاتون تا مرحله آخر بازی‌ها متوجه نمی شن کی براشون این هدیه رو خریده!</p>
					<div class="price">
						<span>Box Price: </span>
						<span>Free !</span>
					</div>
					<a href="/order" class="btn cyan accent-3 btn-large Dark-text">یه کادوی خاص براش بگیر</a>
				
				</div>
				<div class="col l4 m5  s12 right offset-l4 offset-m2">
					<figure class="logo"><img class="fullWidth" src="/images/surprisebox.png" alt=""></figure>
					<p class="white-text bif-font justify-align ">MysteryBox با توجه به نیاز و خواسته های شما، خلاق ترین و جذاب ترین ایده ها و سورپرایزها رو برای مهمونیها و جشن هاتون  پیاده سازی می کنه! آماده اید دوستتون یا مهموناتون رو سورپرایز کنید؟!</p>
					<div class="price">
						<span>Box Price: </span><br><br>
						<span>From 100,000 Toman!</span>
					</div>
					<button data-target="modal1" class="btn modal-trigger cyan accent-3 btn-large Dark-text padding-free">یه جشن یا تولد فوق العاده براش بگیر</button>
					<div id="modal1" class="modal">
						<div class="modal-content Dark-color">
							<form method="POST" action="{{ url('/giftbox')  }}" class="row">
								{{csrf_field()}}
								<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3 padding-free">
									<input id="sender_name2" type="text" class="validate white-text" name="sender_name" required>
									<label for="sender_name2" class="cyan-text text-accent-3"> نام</label>
									<span class="helper-text" data-error="این فیلد رو حتما پر کن" data-success="OK"></span>	
								</div>
								<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3 padding-free">
									<input id="sender_last_name2" type="text" class="validate white-text" name="sender_last_name" required>
									<label for="sender_last_name2" class="cyan-text text-accent-3">نام خانوادگی</label>
									<span class="helper-text" data-error="این فیلد رو حتما پر کن" data-success="OK"></span>	
								</div>
								<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3 padding-free">
									<input id="sender_mobile_number2" type="text" class="validate white-text" name="sender_mobile_number" required>
									<label for="sender_mobile_number2" class="cyan-text text-accent-3"> شماره تماس</label>
									<span class="helper-text" data-error="این فیلد رو حتما پر کن" data-success="OK"></span>	
								</div>
								<div class="col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3 padding-free center-align">
									<br>
									<button class="btn btn-large cyan accent-3 Dark-text">ثبت سفارش</button>
									<br><br><br>
									<div class="cyan-text text-accent-3">
										بعد از ثبت سفارش با شما تماس خواهیم گرفت
									</div>
								</div>
							</form>
						</div>
					</div>
					</div>
				<div class="col l4 m5 s12 right">
					<figure class="logo"><img class="fullWidth" src="/images/mainLogo.png" alt=""></figure>
					<p class="white-text bif-font justify-align ">از زندگی روزمره ت خسته شدی؟ یه تجربه جدید می خوای؟ ما یه بازی خاص فقط مخصوص خودت طراحی می کنیم! اگه آدم ترسویی نیستی MysteryBox رو امتحان کن.</p>
					<div class="price">
						<span>Box Price: </span><br><br>
						<span>From 200,000 Toman!</span>
					</div>
					<button data-target="modal2" class="btn modal-trigger cyan accent-3 btn-large Dark-text">یه بازی تو دنیای واقعی</button>
					<div id="modal2" class="modal">
						<div class="modal-content Dark-color">
							<form method="POST" action="{{ url('/gamebox')  }}" class="row">
								{{csrf_field()}}
								<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3 padding-free">
									<input id="sender_name" type="text" class="validate white-text" name="sender_name" required>
									<label for="sender_name" class="cyan-text text-accent-3"> نام</label>
									<span class="helper-text" data-error="این فیلد رو حتما پر کن" data-success="OK"></span>	
								</div>
								<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3 padding-free">
									<input id="sender_last_name" type="text" class="validate white-text" name="sender_last_name" required>
									<label for="sender_last_name" class="cyan-text text-accent-3">نام خانوادگی</label>
									<span class="helper-text" data-error="این فیلد رو حتما پر کن" data-success="OK"></span>	
								</div>
								<div class="input-field col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3 padding-free">
									<input id="sender_mobile_number" type="text" class="validate white-text" name="sender_mobile_number" required>
									<label for="sender_mobile_number" class="cyan-text text-accent-3"> شماره تماس</label>
									<span class="helper-text" data-error="این فیلد رو حتما پر کن" data-success="OK"></span>	
								</div>
								<div class="col s12 l6 m6 xl6 offset-m3 offset-l3 offset-xl3 padding-free center-align">
									<br>
									<button class="btn btn-large cyan accent-3 Dark-text">ثبت سفارش</button>
									<br><br><br>
									<div class="cyan-text text-accent-3">
										بعد از ثبت سفارش با شما تماس خواهیم گرفت
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
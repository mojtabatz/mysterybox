@extends('layouts.app')
@section('title', 'پایان بازی')
@section('content')
	<section class="Introduction-game">
		<img class="main-logo" src="/images/mainLogo.png" alt="">
		<audio src="/images/gamesMusic.mp3" id="GamesBackgroundMusic" loop autoplay class="displayNone"></audio>
		<div class="container center-align">
			<figure class="user-image">
				<img class="fullWidth" src="{{$user->order->pic_path}}" alt="">
			</figure>
			<br>
			<h1 class="huge-font cyan-text text-accent-3 center-align">نامه دوستت برای تو</h1>
			<br>
			<p class="final-content big-font cyan-text text-accent-3">
				{{$user->order->final_text}}
			</p>
			<a href="/invitation" class="btn btn-large cyan accent-3 Dark-text">پایان بازی</a>
		</div>
	</section>
@endsection
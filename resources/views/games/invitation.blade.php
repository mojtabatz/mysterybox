@extends('layouts.app')
@section('title', 'پایان بازی')
@section('content')
	<section class="Introduction-game">
		<img class="main-logo" src="/images/mainLogo.png" alt="">
		<div class="container center-align">
			<figure class="user-image">
				<img class="fullWidth" src="{{$user->order->pic_path}}" alt="">
			</figure>
			<p class="container paragraph big-font cyan-text text-accent-3 justify-align">
				{{$user->order->receiver_name}} عزیز <br>
				امیدواریم از هدیه ای که گرفتی خوشحال شده باشی. <br>
				پیشنهاد می کنیم که این هدیه رو به صورت کاملا رایگان برای یکی از دوستای صمیمیت، یا دوستای قدیمیت که خیلی وقته یادی ازش نکردی، یا یکی از نزدیکان و آشناهات بفرستی و نشون بدی که به یادشون هستی و خوشحالشون کنی. <br>
				جدا از اینکه اونهارو خوشحال می کنی با این کارت از تیم ما حمایت می کنی و باعث میشی تا ما با امیدواری و قدرت بیشتری به کارمون ادامه بدیم. <br>
				هرچی برای دوستای بیشتری این هدیه رو بفرستی هم خیلی هارو خوشحال می کنی و هم بهترین حامی معنوی MysteryBox میشی. <br>
				از لطفت بی نهایت سپاسگزاریم :)<br>
				از طرف MysteryBox
			</p> 
			<a href="/order" class="btn btn-large cyan accent-3 Dark-text">ارسال برای دوستان</a>
		</div>
	</section>
@endsection
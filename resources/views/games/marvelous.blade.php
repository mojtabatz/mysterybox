@extends('games.gameLayout')
@section('stepNumber', '1')
@section('scripts')
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script src="/js/games/marvelous.js"></script>
@endsection
@section('gameContent')
	<div class="overflow-hidden">
		<h1 class="main-tilte left directionLtr cyan-text text-accent-3">
			<span>Marvelous</span>
			<span>In all things of nature there is something of the marvelous. Aristotle</span>
		</h1>
		<p class="helper right white-text big-font">تو این مرحله باید جواب مسئله رو پیدا کنی. اگه نیاز به راهنمایی داشتی بعد از چند ثانیه یه لامپ باز میشه و می تونی کمک بگیری.</p>
	</div>
	<div class="marvelous row directionLtr">
		<div class="col l3 m3 s12">
			<img class="fullWidth" src="/images/marvelousTip.png" alt="">
		</div>
		<div class="ask col l8  offset-m1 m8  offset-l1 s12 cyan-text text-accent-3">
			<div>Then</div>
			<div class="row">
				<div class="col l2 padding-free m2 s3">What is</div>
				<div class="col l10 padding-free m10 l9 s9"><img class="fullWidth" src="/images/marvelousAsk.png" alt=""></div> 
			</div>
			<div>
				in front of a mirror ?!
			</div>
			<form id="form">
				<div class="input-field col l12  m12 padding-free directionRtl s12 margin-free">
					<input id="MarvelousAnswer" type="text"  class="validate white-text">
					<label for="MarvelousAnswer" class="cyan-text text-accent-3">جوابتو همینجا بنویس</label>
					<span class="helper-text red-text" ></span>
				</div>
				<div class="input-field col l12  m12 padding-free directionRtl s12 center-align">
					<button id="checkAnswer" type="submit" class="cyan btn accent-3 waves-effect Dark-text ">جوابتو چک کن</button>
				</div>
			</form>
			<div id="cangratulation" class="displayNone">
				Mysterybox
			</div>
			<div class="input-field col l12  m12 padding-free directionRtl s12 center-align">
				<a href="/yourself" id="NextLevelButton" class="displayNone">مرحله بعد</a>
			</div>
		</div>
		<div onclick="M.toast({html: 'کلمه xobyretsym  رو برعکس بنویس'})"  class="hint tooltiped">
			<img src="/images/hint.gif" /> <span>راهنمایی !</span>
		</div>
	</div>
@endsection
@extends('games.gameLayout')
@section('stepNumber', '3')
@section('scripts')
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script src="/js/games/success.js"></script>
@endsection
@section('gameContent')
<div>
	<div class="overflow-hidden">
		<h1 class="main-tilte left directionLtr cyan-text text-accent-3">
			<span>Success</span>
			<span>Put your heart, mind, and soul into even your smallest acts. This is the secret of success. Swami Sivananda</span>
		</h1>
		<p class="helper right white-text big-font">
			می تونی ربات رو شکست بدی؟
		</p>
	</div>
	<div class="success">
		<div id="gameComponents">
			<h2 class="white-text big-font">خودت</h2>
			<div style="height: 10px" class="progress">
				<div class="determinate cyan accent-3" id="UserSuccessProgress"></div>
			</div>
			<h2 class="white-text big-font">رقیبت</h2>
			<div style="height: 10px; opacity: .4" class="progress">
				<div class="determinate cyan accent-3" id="VirtualSuccessProgress"></div>
			</div>
			<div class="center-align">
				<a id="SuccessButton" class="btn-floating waves-effect waves-light cyan accent-3 "><i class="material-icons">add</i></a>
			</div>
		</div>
		<div id="SuccessWinning" class="displayNone">
			<h2 class="white-text center-align OpacityAnim">موفق شدی</h2><br><br><br>
			<a href="/truth" class="btn btn-large waves-effect big-font Dark-text cyan accent-3">مرحله بعد</a>
		</div>
		<div class="center-align">
			<div id="SuccessTry" class="displayNone">
				<h2 class="white-text center-align">شکست خوردی! تلاشتو بیشتر کن تا موفق بشی! </h2><br><br><br>
				<button class="btn btn-large waves-effect big-font grey darken-2 cyan-text text-accent-3">دوباره تلاش کن</button>
			</div>
		</div>
		<div onclick="M.toast({html: 'متاسفانه تو این مرحله تنها کمکی که از دستمون بر میاد اینه که از یکی از دوستان و اطرافیانت کمک بگیری :دی'})"  class="hint tooltiped">
			<img src="/images/hint.gif" /> <span>راهنمایی !</span>
		</div>
	</div>
</div>
@endsection
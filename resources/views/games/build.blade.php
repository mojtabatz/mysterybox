@extends('games.gameLayout')
@section('stepNumber', '8')
@section('scripts')
	<script src="/js/games/build.js"></script>
@endsection
@section('gameContent')
<div>
	<div class="overflow-hidden">
		<h1 class="main-tilte left directionLtr cyan-text text-accent-3">
			<span>Build</span>
			<span>We must build dikes of courage to hold back the flood of fear. Martin Luther King, Jr.</span>
		</h1>
		<p class="helper right white-text big-font">
			بساز!
		</p>
	</div>
	<div class="build">
		<div class="buttons center-align">
			<a id="CoursorMover" class="btn-floating waves-effect waves-light cyan accent-3 "></a>
			<a id="LetterActivator" class="btn-floating waves-effect waves-light cyan accent-3 "></a>
		</div>
		<div class="mainText" id="Letters">
			<span class="char">B</span>
			<span class="char">U</span>
			<span class="char">I</span>
			<span class="char">L</span>
			<span class="char">D</span>
			<span class="char">Y</span>
			<span class="char">O</span>
			<span class="char">U</span>
			<span class="char">R</span>
			<span class="char">L</span>
			<span class="char">I</span>
			<span class="char">F</span>
			<span class="char">E</span>
		</div>
		<div class="clear-fix"></div>
		<div class="center-align">
			<a href="/openMind" id="NextLevelButton" class="displayNone">مرحله بعد</a>
		</div>
		<div onclick="M.toast({html: 'با دکمه سمت چپ خونه ها رو رنگ کن و با دکمه سمت راست بین جانمایی حروف حرکت کن. باید تمام حروف هر سه تا کلمه رو کامل رنگ کنی'})" class="hint tooltiped">
			<img src="/images/hint.gif" /> <span>راهنمایی !</span>
		</div>
	</div>
@endsection
@extends('games.gameLayout')
@section('stepNumber', '6')
@section('gameContent')
	<div class="overflow-hidden">
		<h1 class="main-tilte left directionLtr cyan-text text-accent-3">
			<span>Risk</span>
			<span>Be brave. Take risks. Nothing can substitute experience. Paulo Coelho</span>
		</h1>
		<p class="helper right white-text big-font">
			اگه بخوای توی زندگیت موفق بشی یه جاهایی حتما باید ریسک کنی. معلوم نیست این سایت چه تضمینی داره و این
			هدیه از طرف دوست واقعیت برات ارسال شده یا نه. ما یه پیشنهاد برات داریم
			بعد از اینکه دکمه ریسک رو بزنی یه شماره کارت باز میشه و شماره کارت خودت رو هم باید وارد کنی. تو باید 1000 تومن به این کارت واریز کنی
			بعد از اینکه واریز کردی 3000 تومن برات واریز میشه.
			ریسک می کنی؟
		</p>
	</div>
	<div class="center-align clear-fix">
			<div class="row center-align risk-buttons">
				<a class="col offset-l3 offset-m3 l2 m2 s5 waves-effect waves-light btn big-font modal-trigger cyan accent-3 Dark-text" href="#RiskModal">Risk می کنم</a>
				<a href="/build" class="col l2 m2 s5 offset-s2 offset-m1 offset-l1 waves-effect waves-light btn big-font cyan accent-3 Dark-text">مرحله بعد</a>
			</div>
			<div onclick="M.toast({html: 'کلاه برداری نیست. ریسک کن! مطمئن باش اگه می خواستیم کلاه برداری کنیم ازت نمی خواستیم فقط 1000 تومن بریزی! یه ده میلیونی می گفتیم واریز کنی! :دی'})"  class="hint tooltiped">
				<img src="/images/hint.gif" /> <span>راهنمایی !</span>
			</div>
			<div id="RiskModal"  class="modal Dark-color">
				<div class="modal-content">
					<form class="row" method="POST" action="/saveCard">
						{{csrf_field()}}
						<div class="input-field col l6 offset-l3 m6 offset-m3 s10 offset-s1 padding-free">
							<input id="recieverCardNumber" min="1111111111111111" max="9999999999999999" type="number" class="directionLtr white-text" name="card_number" autofocus required>
							<label for="recieverCardNumber" class="cyan-text text-accent-3">شماره کارت خودتو وارد کن</label>
							<span class="helper-text cyan-text text-accent-3 big-font" data-error="این فیلد رو حتما پر کن" data-success="OK">
								به شماره کارت 1597 0597 1610 5054 هزار تومن واریز کن. اعداد رو به انگلیسی وارد کن.
							</span>	
						</div>
						<div class="input-field col l12 m12 s12">
							<button class="btn grey darken-3 cyan-text text-accent-3">ثبت شماره کارت</button>
						</div>
					</form>
				</div>
			</div>
		</div>
@endsection
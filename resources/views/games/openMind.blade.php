@extends('games.gameLayout')
@section('stepNumber', '9')
@section('gameContent')
<div>
	<div class="overflow-hidden">
		<h1 class="main-tilte left directionLtr cyan-text text-accent-3">
			<span>Open</span>
			<span>Open your mind. No One</span>
		</h1>
		<p class="helper right white-text big-font">
			عدد بعدی دنباله رو می تونی حدس بزنی؟
		</p>
	</div>
	<div class="openMind">
		<figure>
			<img src="/images/openMind.jpg" alt="">
		</figure>
		<form id="OpenMindForm" class="row">
			<div class="input-field col l6 offset-l3 m6 offset-m3 s10 offset-s1 padding-free">
				<input id="OpenMindTextInput" type="number" class="validate white-text">
				<label for="OpenMindTextInput" class="cyan-text text-accent-3">جوابتو همینجا بنویس</label>
				<span class="helper-text red-text"></span>
			</div>
			<div class="input-field col l6 offset-l3 m6 offset-m3 s10 offset-s1 center-align">
				<br><br>
				<button type="submit" id="OpenMindChecker" class="btn waves-effect cyan accent-3 Dark-text">جوابتو چک کن</button>
			</div>
		</form>

		<div class="center-align" >
			<div class="displayNone" id="SuccessMessage">
				<div class="huge-font cyan-text text-accent-3 OpacityAnim" style="font-size: 2em;">445</div>
			</div>
			<br>
			<a href="/pagex" id="OPenMindNextLevelButton" class="displayNone">مرحله بعد</a>
			<div onclick="M.toast({html: '... , n1 * 1 + 1، n2 * 2 + 2، n3 * 3 + 3'})"  class="hint tooltiped">
				<img src="/images/hint.gif" /> <span>راهنمایی !</span>
			</div>
			<div class="section-counter white-text">
				تعداد نفراتی که تا این لحظه  این مسئله رو بدون راهنمایی حل کردن :‌ ۷۶ از ۱۸۹۳ نفر
			</div>
		</div>
	</div>
</div>
@endsection
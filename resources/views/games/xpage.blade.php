@extends('games.gameLayout')
@section('stepNumber', '10')
@section('scripts')
	<script src="/js/games/xpageGame.js"></script>
@endsection
@section('gameContent')
<div>
	<div class="overflow-hidden">
		<h1 class="main-tilte left directionLtr cyan-text text-accent-3">
			<span>X</span>
			<span>
				The greatest gift of life is friendship, and I have received it. Hubert H. Humphrey
				And you have received it. X is your friend.
			</span>
		</h1>
		<p class="helper right white-text big-font">
			باید حدس بزنی کدوم دوستت این هدیه رو برات فرستاده. می دونیم که راحت می تونی بفهمی کار کیه!
		</p>
	</div>
	<div class="X-game">
		<div id="XGameSenderName" class="displayNone">{{str_replace(" ","",$user->order->sender_name)}}</div>
			<div id="XGameAnswerWrap" class="answer"></div>
			<div id="XGameWinnigMessage" class="displayNone">
				<div class="huge-font OpacityAnim"> {{$user->order->sender_name}}</div>
				<a href="/final" class="btn btn-large big-font cyan accent-3 Dark-text">مرحله بعد</a>
			</div>
			<div id="XGameWrapper">
				<div class="letters-wrap">
					<div class="letters">
						<span data-letter="ا" class="big-font">ا</span>
						<span class="big-font" data-letter="ب">ب</span>
						<span class="big-font" data-letter="پ">پ</span>
						<span class="big-font" data-letter="ت">ت</span>
						<span class="big-font" data-letter="ث">ث</span>
						<span class="big-font" data-letter="ج">ج</span>
						<span class="big-font" data-letter="چ">چ</span>
						<span class="big-font" data-letter="ح">ح</span>
					</div>
					<div class="letters">
						<span class="big-font" data-letter="خ">خ</span>
						<span class="big-font" data-letter="د">د</span>
						<span class="big-font" data-letter="ذ">ذ</span>
						<span class="big-font" data-letter="ر">ر</span>
						<span class="big-font" data-letter="ز">ز</span>
						<span class="big-font" data-letter="ژ">ژ</span>
						<span class="big-font" data-letter="س">س</span>
						<span class="big-font" data-letter="ش">ش</span>
					</div>
					<div class="letters">
						<span class="big-font" data-letter="ص">ص</span>
						<span class="big-font" data-letter="ض">ض</span>
						<span class="big-font" data-letter="ط">ط</span>
						<span class="big-font" data-letter="ظ">ظ</span>
						<span class="big-font" data-letter="ع">ع</span>
						<span class="big-font" data-letter="غ">غ</span>
						<span class="big-font" data-letter="ف">ف</span>
						<span class="big-font" data-letter="ق">ق</span>
					</div>
					<div class="letters">	
						<span data-letter="ک" class="big-font">ک</span>
						<span data-letter="گ" class="big-font">گ</span>
						<span data-letter="ل" class="big-font">ل</span>
						<span data-letter="م" class="big-font">م</span>
						<span data-letter="ن" class="big-font">ن</span>
						<span data-letter="و" class="big-font">و</span>
						<span data-letter="ه" class="big-font">ه</span>
						<span data-letter="ی" class="big-font">ی</span>
					</div>
				</div>
				<div class="center-align">
					<button class="btn waves-effect cyan-text text-accent-3 grey darken-3 remove-form">پاک کردن فرم</button>
				</div>
				<div onclick="M.toast({html: 'اصلا دوست نداریم این مرحله رو راهنماییت کنیم ولی اگه نفهمیدی مجبوریم دیگه بگیم. دوستت {{$user->order->sender_name}} هستش!'})"  class="hint tooltiped">
					<img src="/images/hint.gif" /> <span>راهنمایی !</span>
				</div>
			</div>
	</div>
</div>
</div>
@endsection
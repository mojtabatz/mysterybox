@extends('layouts.app')
@section('title', 'MysteryBox')
@section('metaData')
	@yield('scripts')
@endsection
@section('content')
	<div class="Games-layout">
		<header class="header">
			<a class="cyan-text text-accent-3 big-font" href="#">{{$user->order->receiver_name}}</a>
			<a class="cyan-text text-accent-3 big-font" href="/logout">Log out</a>
		</header>
		<audio src="/images/gamesMusic.mp3" id="GamesBackgroundMusic" autoplay loop class="displayNone"></audio>
		<main class="row">
			<aside class="sidebar col l2 m2 s12">
				<figure class="sidebar-image">
					<img class="fullWidth" src="/images/mainLogo.png" alt="">
				</figure>
				<ul data-step=@yield('stepNumber') id="GameSteps" class="steps directionLtr">
					<li>
						<span class="big-font center-align">M</span>
						<span class="white-text big-font">Marvelous</span>
					</li>
					<li>
						<span class="big-font center-align">Y</span>
						<span class="white-text big-font">Yourself</span>
					</li>
					<li>
						<span class="big-font center-align">S</span>
						<span class="white-text big-font">Success</span>
					</li>
					<li>
						<span class="big-font center-align">T</span>
						<span class="white-text big-font">Truth</span>
					</li>
					<li>
						<span class="big-font center-align">E</span>
						<span class="white-text big-font">Enjoy</span>
					</li>
					<li>
						<span class="big-font center-align">R</span>
						<span class="white-text big-font">Risk</span>
					</li>
					<li>
						<span class="big-font center-align">Y</span>
						<span class="white-text big-font">Yourself</span>
					</li>
					<li>
						<span class="big-font center-align">B</span>
						<span class="white-text big-font">Build</span>
					</li>
					<li>
						<span class="big-font center-align">O</span>
						<span class="white-text big-font">Open</span>
					</li>
					<li>
						<span class="big-font center-align">X</span>
						<span class="white-text big-font">X</span>
					</li>
				</ul>
			</aside>
			<section class="main-content col l10 m10 s12">
				@yield('gameContent')
			</section>
		</main>
		
	</div>
@endsection
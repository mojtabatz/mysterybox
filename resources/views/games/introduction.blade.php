@extends('layouts.app')
@section('title', 'معرفی بازی')
@section('content')
	<section class="Introduction-game">
		<img class="main-logo" src="/images/mainLogo.png" alt="">
		<div class="container center-align">
			<figure class="user-image">
				<img class="fullWidth" src="{{$user->order->pic_path}}" alt="">
				<figcaption class="cyan-text text-accent-3 big-font">سلام {{$user->order->receiver_name}}</figcaption>
			</figure>
			<p class="big-font cyan-text text-accent-3 center-align">
				به MysteryBox خوش اومدی !<br><br><b></b>
			</p>
			<p class="main-content big-font cyan-text text-accent-3">
				الان که این صفحه رو می بینی به این معنیه که تو آدم خاصی هستی.
				یکی از دوستات این هدیه خاص رو فقط برای تو خریده و می خواد که خوشحالت کنه.
				9 تا مرحله پیش رو داری و توی مرحله آخر متوجه میشی که چه کسی برات این هدیه
				رو تهیه کرده. راستی برات یه نامه هم فرستاده که آخرش می بینی! <br>
				موفق باشی ;)
			</p>
			<a href="/marvelous" class="btn btn-large cyan accent-3 Dark-text">شروع بازی</a><br><br>
			<span class="cyan-text text-accent-3">زمان تقریبی بازی : بین 15 تا 30 دقیقه</span>
			<br><br><br>
		</div>
	</section>
@endsection
@extends('games.gameLayout')
@section('stepNumber', '4')
@section('scripts')
	<script src="/js/games/truth.js"></script>
@endsection
@section('gameContent')
<div>
	<div class="overflow-hidden">
		<h1 class="main-tilte left directionLtr cyan-text text-accent-3">
			<span>Truth</span>
			<span>Three things cannot be long hidden: the sun, the moon, and the truth. Buddha</span>
		</h1>
		<p class="helper right white-text big-font">
			حقیقت رو می تونی پیدا کنی؟
		</p>
	</div>
	<div>
		<div class="truth row">
			<div class="secret" id="UserName">{{$user->order->receiver_name}}</div>
			<form id="TruthForm"> 
				<div  class="input-field col l6 offset-l3 m6 offset-m3 s10 offset-s1 padding-free">
					<input id="TruthInput" type="text" class="validate white-text" >
					<label for="TruthInput" class="cyan-text text-accent-3">حقیقتی که میبینی رو بنویس</label>
					<span class="helper-text red-text"></span>
				</div>
				<div class="input-field col l6 offset-l3 m6 offset-m3 s12 padding-free center-align">
					<button type="submit" id="TruthChecker" class="btn waves-effect cyan accent-3 Dark-text">
						جوابتو چک کن
					</button>
				</div>
			</form>
			<div id="SuccessMessage" class="displayNone">
				<a href="/enjoy" class="btn btn-large big-font waves-effect cyan accent-3 Dark-text">مرحله بعد</a>
			</div>
			<div onclick="M.toast({html: 'نور صفحتو زیاد کن. یه کلمه وسط صفحه نوشته شده که اگه دقت کنی پیداش می کنی'})"  class="hint tooltiped">
				<img src="/images/hint.gif" /> <span>راهنمایی !</span>
			</div>
		</div>
	</div>
</div>
@endsection
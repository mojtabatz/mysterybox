@extends('games.gameLayout')
@section('stepNumber', '2')
@section('scripts')
	<script src="/js/games/yourself.js"></script>
@endsection
@section('gameContent')
	<div class="overflow-hidden">
		<h1 class="main-tilte left directionLtr cyan-text text-accent-3">
			<span>Y</span>
			<span>Be yourself; everyone else is already taken. Oscar Wilde</span>
		</h1>
		<p class="helper right white-text big-font">خودتو پیدا کن!</p>
	</div>
	<div class="yourself">
		<div class="image-box hide-on-small-only show-on-medium-and-up center-align">
			<img onload="showUserImage();" src="/images/desktop_yourselfGame.jpg" alt="">
			<div id="desktopUserImage" class="displayNone"><img class="fullWidth" src="{{$user->order->pic_path}}" alt=""></div>
		</div>
		<div class="image-box hide-on-med-and-up  show-on-small center-align">
			<img src="/images/mobile_yourselfGame.jpg" onload="showUserImage();"  alt="">
			<div id="mobileUserImage" class="displayNone"><img class="fullWidth" src="{{$user->order->pic_path}}" alt=""></div>
		</div>
		<div id="cangratulation" class="displayNone">
			<figure class="final-image"><img class="fullWidth OpacityAnim" src="{{$user->order->pic_path}}" alt=""></figure>
			<a href="/success" class="btn btn-large big-font waves-effect cyan accent-3 Dark-text">مرحله بعد</a>
		</div>
		<div onclick="M.toast({html: 'گوشه پایین سمت راست صفحه روی عکست کلیک کن'})"  class="hint tooltiped">
			<img src="/images/hint.gif" /> <span>راهنمایی !</span>
		</div>
	</div>
@endsection
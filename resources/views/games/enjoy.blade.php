@extends('games.gameLayout')
@section('stepNumber', '5')
@section('scripts')
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script src="/js/games/enjoy.js"></script>
@endsection
@section('gameContent')
<div>
	<div class="overflow-hidden">
		<h1 class="main-tilte left directionLtr cyan-text text-accent-3">
			<span>Enjoy</span>
			<span>Be happy for this moment. This moment is your life. Omar Khayyam</span>
		</h1>
		<p class="helper right white-text big-font">
			دوستت برات یه آهنگ آپلود کرده و دوست داره حتما به این آهنگ گوش کنی و از لحظه ات لذت ببری. فقط گوش کن و لذت ببر 
		</p>
		<audio  preload="auto" id="EnjoyMusicComponent" src="{{$user->order->music_path}}"></audio>
	</div>
	<div class="enjoy center-align">
		<div id="MusicPreLoader" class="preloader-wrapper big active">
			<div class="spinner-layer spinner-cyan-only accent-3 directionLtr" id="MusicPreLoader">
				<div class="circle-clipper left">
					<div class="circle"></div>
				</div>
				<div class="gap-patch">
					<div class="circle"></div>
				</div>
				<div class="circle-clipper right">
					<div class="circle"></div>
				</div>
			</div>
		</div>
		<button id="EnjoyMusicPlayer" class="displayNone"></button>
		<div style="height: 10px" class="progress">
			<div class="determinate cyan accent-3" id="MusicProgress"></div>
		</div>
		<div id="NextLevelButton" class="displayNone">
			<br><br><br>
			<a href="/risk" class="btn btn-large waves-effect big-font Dark-text cyan accent-3">مرحله بعد</a>
		</div>
		<div onclick="M.toast({html: 'خداییش این مرحله رو هم راهنمایی می خوای؟! فقط از موزیک لذت ببر!'})"  class="hint tooltiped">
			<img src="/images/hint.gif" /> <span>راهنمایی !</span>
		</div>
	</div>
</div>
@endsection
@extends('layouts.app')
@section('pageTitle', 'MysteryBox | Login')
@section('content')
<div class="Login">
	<div class="container">
		 <div class="row center-align">
			 <figure>
				<img class="fullWidth" src="/images/mainLogo.png" alt="">
			</figure>
			  <div class="col m4 l4 offset-l4 offset-m4 s12 directionLtr">
					<form method="POST" action="{{ url('/login') }}">
						@csrf
						<div class="input-field">
								<input id="username" type="text" class="validate white-text" name="username" required>
								<label for="username" class="cyan-text text-accent-3">{{ __('User Name') }}</label>
								<span class="helper-text" data-error="Required" data-success="OK"></span>
								@if ($errors->has('email'))
									<span class="displayNone">{{ $errors->first('email') }}</span>
								@endif
						</div>
						<div class="input-field">
							<input id="password" type="password" class="validate white-text" name="password" required>
							<label for="password" class=" cyan-text text-accent-3">{{ __('Password') }}</label>
							<span class="helper-text" data-error="Required" data-success="OK"></span>
							@if ($errors->has('password'))
								{{ $errors->first('password') }}
							@endif
						</div>
						<div class="form-group">
							<button type="submit" class="btn cyan-text text-accent-3 waves-effect fullWidth grey darken-3">
								{{ __('Login') }}
							</button>
						</div>
					</form>
			  </div>
		 </div>
	</div>
</div>
@endsection

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<link rel="icon" href="/images/favicon.png"  sizes="32x32">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#00e5ff">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#00e5ff">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#00e5ff">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield('pageTitle')</title>
	<script  src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
	<script>
		(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-5LCJD66');
	</script>

	<script src="{{ asset('js/app.js') }}" defer></script>
	@yield('metaData')
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
	<div id="app">
		<main class="py-4">
			@yield('content')
		</main>
	</div>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5LCJD66"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
</body>
</html>

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('auth.login');
})->name('login');

Route::get('/login', function () {
    return view('auth.login');
});

Route::post('/login',function(){
    if(Auth::attempt(['email'=>request()->username,'password'=>request()->password])){
        $user = Auth::user();
        if(!empty($user->order)) {
            return redirect()->route('introduction');
        } else {
            return redirect()->route('view_orders');
        }
    } else {
        return view('auth.login');
    }
});
Route::get('/introduction',function(){
    $user = Auth::user();
    return view('games.introduction',compact('user'));
})->middleware('auth')->name('introduction');

Route::post('/order',function(){
    request()->validate([
            'file' => 'required|image',
            'music' => 'mimes:mpga,wav,ogg|max:20000',
    ]);
    $file = request()->file('file');
    $fileName = time().'.'.$file->getClientOriginalExtension();
    $music = request()->file('music');
    $musicName = time().'.'.$music->getClientOriginalExtension();
    $file->move(public_path('pics'),$fileName);
    $music->move(public_path('musics'),$musicName);
    $picPath = 'pics/'.$fileName;
    $musicPath = 'musics/'.$musicName;
    $gifts = '';
    $data = array_except(request()->all(),['gifts','_token','file','month','day','hour']);
    if(request()->has('gifts')) {
        foreach (request()->gifts as $gift) {
            $gifts .= $gift . ',';
        }
        $data['gifts'] = $gifts;
    }
    $shamsiGiftTime = '1397/'.request()->month.'/'.request()->day;
    $convert = \App\Mysterybox\ShamsiCalender\Shamsi::convertToGeorgian($shamsiGiftTime);
    $time = \Carbon\Carbon::parse($convert)->setTime(request()->hour,0);
    $data['gift_time'] = $time;
    $data['pic_path'] = $picPath;
    $data['music_path'] = $musicPath;
    \App\Order::create($data);
    return view('order.checkout');
});
Route::get('checkout',function(){
        return view('order.checkout');
});

Route::get('newuser',function(){
    if(empty($user->order)) {
        return view('auth.register');
    }
})->middleware('auth')->name('register');

Route::post('saveCard',function(){
    $user = Auth::user();
    $user->card_number = request()->card_number;
    $user->save();
    return redirect()->route('build');
})->middleware('auth');
Route::post('newuser',function(){
    $user = \App\User::create([
        'name' => request()->name,
        'email' => request()->email,
        'password' => bcrypt(request()->password)
    ]);
    $order = \App\Order::find(request()->order_id);
    $order->receiver_user_id = $user->id;
    $order->save();
    return view('auth.register');
});
Route::post('/giftbox',function(){
    $data = array_except(request()->all(),['_token']);
//    $data['sender_name'];
//    $data['sender_last_name'];
//    $table->string['sender_mobile_number');
    $data['receiver_name'] = "";
    $data['receiver_last_name'] = "";
    $data['receiver_mobile_number'] = "";
    $data['custom_gift'] = "giftbox";
    $data['final_text'] = "";
    $data['gift_time'] = \Carbon\Carbon::now();
    $data['pic_path'] = "";
    $data['music_path'] = "";
    \App\Order::create($data);
    return redirect()->route('plans');
});
Route::post('/gamebox',function(){
    $data = array_except(request()->all(),['_token']);
//    $data['sender_name'];
//    $data['sender_last_name'];
//    $table->string['sender_mobile_number');
    $data['receiver_name'] = "";
    $data['receiver_last_name'] = "";
    $data['receiver_mobile_number'] = "";
    $data['custom_gift'] = "gamebox";
    $data['final_text'] = "";
    $data['gift_time'] = \Carbon\Carbon::now();
    $data['pic_path'] = "";
    $data['music_path'] = "";
    \App\Order::create($data);
    return redirect()->route('plans');
});
Route::get('/order', function () {
    return view('order.order');
});
//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/marvelous', function () {
    $user = Auth::user();
    return view('games.marvelous',compact('user'));
})->middleware('auth')->name('marvelous');
Route::get('/risk', function () {
    $user = Auth::user();
    return view('games.risk',compact('user'));
})->middleware('auth')->name('risk');

Route::get('/truth', function () {
    $user = Auth::user();
    return view('games.truth',compact('user'));
})->middleware('auth')->name('truth');

Route::get('games/mathbattle',function (){
    $data = [];
    for ($i = 0 ; $i<100 ; $i ++) {
        $characters = '+-*/';
        $charactersLength = strlen($characters);
        $randomOperator = $characters[rand(0, $charactersLength - 1)];
        if ($randomOperator == '+') {
            $firstNumber = rand(0, 199);
            $secondNumber = rand(0, $firstNumber);
            $truness = rand(0, 1);
            $result = 0;
            if ($truness == 1) {
                $result = $firstNumber + $secondNumber;
            } else {
                $condition = rand(1, 3);
                if ($condition == 1) {
                    $cond = rand(1, 2);
                    if ($cond == 1) {
                        $result = $firstNumber + $secondNumber + 1;
                    } else {
                        $result = $firstNumber + $secondNumber - 1;
                    }
                } elseif ($condition == 2) {
                    $cond = rand(1, 2);
                    if ($cond == 1) {
                        $result = $firstNumber + $secondNumber + 10;
                    } else {
                        $result = $firstNumber + $secondNumber - 10;
                    }
                } elseif ($condition == 3) {
                    $result = $firstNumber - $secondNumber;
                }
            }
            $data[] = [$firstNumber , $randomOperator , $secondNumber , $result , boolval($truness)];
        } elseif ($randomOperator == '-') {
            $firstNumber = rand(0, 199);
            $secondNumber = rand(0, $firstNumber);
            $truness = rand(0, 1);
            $result = 0;
            if ($truness == 1) {
                $result = $firstNumber - $secondNumber;
            } else {
                $condition = rand(1, 3);
                if ($condition == 1) {
                    $cond = rand(1, 2);
                    if ($cond == 1) {
                        $result = $firstNumber - $secondNumber + 1;
                    } else {
                        $result = $firstNumber - $secondNumber - 1;
                    }
                } elseif ($condition == 2) {
                    $cond = rand(1, 2);
                    if ($cond == 1) {
                        $result = $firstNumber - $secondNumber + 10;
                    } else {
                        $result = $firstNumber - $secondNumber - 10;
                    }
                } elseif ($condition == 3) {
                    $result = $firstNumber + $secondNumber;
                }
            }
            $data[] = [$firstNumber , $randomOperator , $secondNumber , $result , boolval($truness)];

        } elseif ($randomOperator == '/'){
            $divisors = [];
            $firstNumber = 0 ;
            while (empty($divisors)) {
                $firstNumber = rand(2, 199);
                for ($j = 2; $j <= sqrt($firstNumber); $j++) {
                    if ($firstNumber % $j == 0) {
                        if ($firstNumber / $j == $j) {
                            $divisors[] = $j;
                        }
                        else {
                            $divisors [] = $j;
                            $divisors[] = $firstNumber / $j;
                        }
                    }
                }
            }
            $secondNumber =  $divisors[array_rand($divisors)];
            $truness = rand(0, 1);
            $result = 0;
            if ($truness == 1) {
                $result = $firstNumber / $secondNumber;
            } else {
                $condition = rand(1, 2);
                if ($condition == 1) {
                    $cond = rand(1, 2);
                    if ($cond == 1) {
                        $result = ($firstNumber / $secondNumber) + 1;
                    } else {
                        $result = ($firstNumber / $secondNumber) - 1;
                    }
                } elseif ($condition == 2) {
                    $cond = rand(1, 2);
                    if ($cond == 1) {
                        $result = ($firstNumber / $secondNumber) + 10;
                    } else {
                        $result = ($firstNumber / $secondNumber) - 10;
                    }
                }
            }
            $data[] = [$firstNumber , $randomOperator , $secondNumber , $result , boolval($truness)];

        } elseif ($randomOperator == '*') {
            $firstNumber = rand(0, 199);
            $secondNumber = rand(0, 199);
            $truness = rand(0, 1);
            $result = 0;
            if ($truness == 1) {
                $result = $firstNumber * $secondNumber;
            } else {
                $condition = rand(1, 2);
                if ($condition == 1) {
                    $cond = rand(1, 2);
                    if ($cond == 1) {
                        $result = $firstNumber * $secondNumber + 1;
                    } else {
                        $result = $firstNumber * $secondNumber - 1;
                    }
                } elseif ($condition == 2) {
                    $cond = rand(1, 2);
                    if ($cond == 1) {
                        $result = $firstNumber * $secondNumber + 10;
                    } else {
                        $result = $firstNumber * $secondNumber - 10;
                    }
                }
            }
            $data[] = [$firstNumber , $randomOperator , $secondNumber , $result , boolval($truness)];
        }
    }
    dd($data);
//    return view('games.truth',compact('user'));


});

Route::get('/logout',function() {
    Auth::logout();
    return redirect()->route('login');
});
Route::get('/openMind', function () {
    $user = Auth::user();
    return view('games.openMind',compact('user'));
})->middleware('auth')->name('open');
Route::get('/success', function () {
    $user = Auth::user();
    return view('games.success',compact('user'));
})->middleware('auth')->name('success');
Route::get('/enjoy', function () {
    $user = Auth::user();
    return view('games.enjoy',compact('user'));
})->middleware('auth')->name('enjoy');
Route::get('/pagex', function () {
    $user = Auth::user();
    return view('games.xpage',compact('user'));
})->middleware('auth')->name('xpage');
Route::get('/build', function () {
    $user = Auth::user();
    return view('games.build',compact('user'));
})->middleware('auth')->name('build');
Route::get('/final', function () {
    $user = Auth::user();
    return view('games.final',compact('user'));
})->middleware('auth')->name('final');
Route::get('/invitation', function () {
    $user = Auth::user();
    return view('games.invitation',compact('user'));
})->middleware('auth')->name('invitation');
Route::get('/yourself', function () {
    $user = Auth::user();
    return view('games.yourself',compact('user'));
})->middleware('auth')->name('yourself');


Route::get('/plans', function () {
    return view('games.plans');
})->name('plans');

Route::get('/orders', function () {
    $user = Auth::user();
    if(empty($user->order)) {
        $orders = \App\Order::all();
        return view('order.ordersTable', compact('orders'));
    }
})->middleware('auth')->name('view_orders');;

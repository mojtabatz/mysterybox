<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sender_name');
            $table->string('sender_last_name');
            $table->string('sender_mobile_number');
            $table->string('receiver_name');
            $table->string('receiver_last_name');
            $table->string('receiver_mobile_number');
            $table->string('receiver_address')->nullable();
            $table->string('gifts')->nullable();
            $table->string('custom_gift')->nullable();
            $table->text('final_text');
            $table->timestamp('gift_time');
            $table->string('pic_path');
            $table->string('music_path');
            $table->integer('receiver_user_id')->unsigned()->nullable();
            $table->foreign('receiver_user_id')
                ->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

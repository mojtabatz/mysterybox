$(document).ready(function(){
	$('select').formSelect();
	$('.tooltipped').tooltip();
	$('.modal').modal();
});

//**************************** Game Side bar Handling ***************************************
var sideBarStepChildren = $('#GameSteps').children();
var stepNumber = Number($('#GameSteps').attr('data-step'));
for (var i = sideBarStepChildren.length - 1; i >= stepNumber ; i--) {
	$('#GameSteps').children()[i].remove();
}


//*************************************  Open mind checker ***********************************
$('#OpenMindForm').submit(function(e) {
	e.preventDefault();
	if (Number($('#OpenMindTextInput').val()) == 445 ) {
		$(this).attr('class', 'displayNone');
		$('#SuccessMessage').attr('class', '');
		$('#OPenMindNextLevelButton').attr('class', 'btn waves-effect cyan accent-3 big-font grey-text text-darken-3 btn-large');
	} else {
		$('.helper-text').html('نه اشتباهه دوباره سعی کن');
	}
});
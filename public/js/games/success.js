var counter = 1;
var altCounter = 1;
var virtualInterval, timeOutVar;
$(document).ready(function() {
	$('#SuccessButton').click(function() {
		if(counter == 1) {
			startTimeOut();
			virtualInterval = setInterval(() => {
				altCounter++;
				document.getElementById('VirtualSuccessProgress').style.width = `${altCounter}%`;
			}, 180);
		}
		if (counter <= 100) {
			counter++;
			document.getElementById('UserSuccessProgress').style.width = `${counter}%`;
		} else {
			clearTimeout(timeOutVar);
			$('#gameComponents').addClass('displayNone');
			$('#SuccessWinning').attr('class', 'center-align');
			clearInterval(virtualInterval);
		}
	});
	$('#SuccessTry').click(function() {
		$('#gameComponents').removeClass('displayNone');
		$('#SuccessTry').addClass('displayNone');
		document.getElementById('VirtualSuccessProgress').style.width = `0%`;
		document.getElementById('UserSuccessProgress').style.width = `0%`;
		counter = 1;
		altCounter = 1;
	});

});
function startTimeOut() {
	timeOutVar = setTimeout(function() {
		$('#gameComponents').addClass('displayNone');
		$('#SuccessTry').addClass('center-align');
		$('#SuccessTry').removeClass('displayNone');
		clearInterval(virtualInterval);
	}, 18000)
}
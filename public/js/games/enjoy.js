$(document).ready(function(){
	var musicDuration;
	var audio = document.getElementById("EnjoyMusicComponent");
	audio.play();
	audio.pause();
	audio.onloadedmetadata = function() {
		musicDuration = audio.duration;
	};
	var isPlayed = false;
	var musicProgress, percentCounter = 0;
	audio.addEventListener('canplay', function() { 
		$("#EnjoyMusicPlayer").attr('class', 'playButton pulse');
		$("#MusicPreLoader").remove();
	}, false);
	$('#EnjoyMusicPlayer').click(function(){
		if (isPlayed) {
			audio.pause();
			this.className = 'playButton'
			clearInterval(musicProgress);
			isPlayed = false;
			document.getElementById('GamesBackgroundMusic').play();
		} else {
			audio.play();
			document.getElementById('GamesBackgroundMusic').pause();
			this.className = 'playButton paused';
			musicProgress = setInterval(() => {
				percentCounter++;
				if (percentCounter == 100) {
					handleMusicEnd();
				}
				document.getElementById('MusicProgress').style.width = `${percentCounter}%`;
			}, (musicDuration / 100) * 1000)
			isPlayed = true;
		}
	});
	function handleMusicEnd() {
		clearInterval(musicProgress);
		$('#NextLevelButton').attr('class', '');
		$('#EnjoyMusicPlayer').attr('class', 'playButton');
		document.getElementById('MusicProgress').style.width = `0%`;
		percentCounter = 0;
	}
});
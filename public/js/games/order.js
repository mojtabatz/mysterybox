$(document).ready(function() {
	jQuery.validator.setDefaults({
		success: "valid"
	});
	jQuery.extend(jQuery.validator.messages, {
   		required: "",
   	});
	jQuery.validator.addMethod("persianRegex", function(value, element) {
	  return this.optional(element) || /^[آ-ی\s]*$/.test(value);
	}, "لطفا فقط حروف فارسی وارد نمایید.");
	$( "#OrderForm" ).validate({
		rules: {
			sender_name: {
				required: true,
				persianRegex: true
			},
			sender_last_name: {
				required: true,
				persianRegex: true
			},
			receiver_name: {
				required: true,
				persianRegex: true
			},
			receiver_last_name: {
				required: true,
				persianRegex: true
			},
			sender_mobile_number: {
				required: true,
				number: true
			},
			receiver_mobile_number: {
				required: true,
				number: true
			},
			file: {
				required: true,
				extension: 'jpg|pmb|png|svg|jpeg|gif'
			},
			music: {
				required: true,
				extension: 'mp3|wav|ogg'
			}
		},
		messages: {
			sender_mobile_number: {
				number: 'لطفا شماره موبایل صحیح خود را به انگلیسی وارد نمایید'
			},
			receiver_mobile_number: {
				number: 'لطفا شماره موبایل صحیح خود را به انگلیسی وارد نمایید'
			},
			file:  'لطفا فایل تصویر مناسب را انتخاب کنید.',
			music: 'لطفا فایل موسیقی مناسب را انتخاب کنید.'
		}
	});
	$('#OrderForm').submit(function(e) {
		if ($(this).valid()) {
			$('#OrderSubmitButton').remove();
			$('#Preloader').attr('class', '');
		}
	});
	var userName = $('input#customerFriendName').val();
	var customerName = $('input#customerName').val();
	var selectedGiftText = '';
	$('input#customerFriendName').change(function() {
		userName = this.value;
		updateGiftTextInformation();
	});
	$('input#customerName').change(function() {
		customerName = this.value;
		updateGiftTextInformation();
	});
	$('input[data-type="GiftTemplate"').change(function() {
		var giftTexts = {
		TemplateOne: `${userName} عزیز سلام
دوست داشتم یه یادی ازت بکنم و می خواستم یه ذره سورپرایز بشی. امیدوارم هدیه ای که برات فرستادم باعث شده باشه تا یه ذره از خستگی کارای روزمره ات کم کرده باشه و حالت رو خوب کرده باشه. خیلی دوست دارم حضوری یه برنامه بذاریم و ببینمت.
خواستم بهت یادآوری کنم که فراموشت نکردم و همیشه به یادت هستم رفیق گلم 
دوستدار تو
${customerName}`,
	TemplateTwo: `سلام ${userName} عزیزم
امیدوارم حالت خوب باشه. دوست داشتم یه هدیه خاص برات تهیه کنم و دیدم چی بهتر از این هدیه. خیلی دلم برات تنگ شده و دوست دارم ببینمت. یه روز با هم هماهنگ کنیم و یه برنامه بیرون دو تایی یا با دوستای قدیمیمون بذاریم حتما.
امیدوارم خوشحال شده باشی بابت این هدیه.
دوستدار تو
${customerName}`,
	TemplateThree: `سلام ${userName}
دوست داشتم یه هدیه خاص واسه دوست فوق العادم بفرستم تا بگم که خیلی دوست دارم دوست جونم.
امیدوارم خوشحال شده باشی. ناقابل بود.
دوست اول و آخر تو 
${customerName}`,
	TemplateFour: `${userName} جان سلام
شدیدا دلم برات تنگ شده ولی نتونستم بیام ببینمت. یه هدیه کوچیک بود که امیدوارم خوشحال شده باشی. 
همیشه لبت خندون باشه و دلت شاد و تنت سلامت
دوستدار تو 
${customerName}`,
	TemplateFive: `${userName} سلاااااااااااااامممممممممممم
چطورییییییییییی؟!؟!؟؟ خیلی دلم برات تنگ شده بود می خواستم یه هدیه خیلی خاص برات بفرستم تا بدونی که چقد برای من خاص هستیییییی. دلتنگتم شدیییییددددد. از اونجایی که من خیلی خفنم مطمئنم از هدیه ای که برات فرستادم خیلی خوشحال شدی الان :دی 
ببینمت رفیییییقققققق
${customerName}`,
	TemplateSix: `همسر عزیززززممممم سلاممممم عشقققققممممممم
می دونی که چقددددد عاشقتممممم و دیوونتممممم. می خواستم یه کادوی خاص و جدید بهت بدممممم که تا حالا هیچکی بهت ندادهههههه. امیدوارم دوست داشته باشی و حتما باید دوست داشته باشییییی :))))
خیلی خیلی خیلی دوستتتت دارم همسر جااااااااااااان.
دوسسسسسسسسست دارم سبد سبد
باااااااااازززز گل ععععشششششقققق جووونه زززززززدد
دوسسسسسسست دارم یه عالمههههههه
هههررررررر چییییی بگمممممممم بازم کممههههههههه
عشق اول و آخرت
${customerName}`,
	TemplateSeven: `سلام ${userName} عزیزم
می خواستم یه کادوی جدید و خاص به خاص ترین آدم دنیام بدم. خیلی دوست دارم و امیدوارم تونسته باشه مهربون ترین همسر دنیا رو یه کوچولو خوشحال کرده باشم.
عاشقانه عاشقتم عزیزم.
${customerName}`,
	TemplateEight: `${userName} عزیز سلام
دوست داشتم برای تولدت خاص ترین کادو رو بفرستم. واسه همین تصمیم گرفتم
MysteryBox رو انتخاب کنم.
امیدوارم تونسته باشم متفاوت ترین کادویی که تاحالا واسه تولدت گرفته بودی رو بهت هدیه داده باشم.
بهترین ها رو برات آرزو می کنم. امیدوار همیشه شاد و سلامت و پیروز باشی
به یادت هستم
${customerName}`,
	TemplateNine: `${userName}  سلاممممممممممممممم
توللللدددددددد توللللللللللللللللد تولللللللللللدت مباررررررررررررررررک
مباررررررررررک مباررررررررررررررررک تولدت مبارررررررررررررکککک
لبت شااااااااااااد و دلت خوووووووووووووش چو گل پرخنده باشییییییییییییی
بیا ششششمعارو فوت کننننننن که صد سال زنده باشییییییییییییییی
دیری دیرین دیری دیرین
دیریری دیریری دیری دیرین
عاشقتممممممم
از طرف ${customerName}`,
	TemplateTen: `سلام ${userName} عزیز
در این سالروز زمینی شدنت بهترین دعاها را برایت می کنم و آرزوی سلامتی، تندرستی و موفقیت برایت دارم.
همیشه سربلند و پایدار باشی
دوستدار تو
${customerName}`,
	TemplateFree: ''
};
	$('textarea#GiftText').val(giftTexts[$(this).attr('id')]);
	selectedGiftText = $(this).attr('id');
	M.textareaAutoResize($('textarea#GiftText'));
	document.getElementById("GiftText").focus();
});

function updateGiftTextInformation() {
		var giftTexts = {
	TemplateOne: `${userName} عزیز سلام
دوست داشتم یه یادی ازت بکنم و می خواستم یه ذره سورپرایز بشی. امیدوارم هدیه ای که برات فرستادم باعث شده باشه تا یه ذره از خستگی کارای روزمره ات کم کرده باشه و حالت رو خوب کرده باشه. خیلی دوست دارم حضوری یه برنامه بذاریم و ببینمت.
خواستم بهت یادآوری کنم که فراموشت نکردم و همیشه به یادت هستم رفیق گلم 
دوستدار تو
${customerName}`,
	TemplateTwo: `سلام ${userName} عزیزم
امیدوارم حالت خوب باشه. دوست داشتم یه هدیه خاص برات تهیه کنم و دیدم چی بهتر از این هدیه. خیلی دلم برات تنگ شده و دوست دارم ببینمت. یه روز با هم هماهنگ کنیم و یه برنامه بیرون دو تایی یا با دوستای قدیمیمون بذاریم حتما.
امیدوارم خوشحال شده باشی بابت این هدیه.
دوستدار تو
${customerName}`,
	TemplateThree: `سلام ${userName}
دوست داشتم یه هدیه خاص واسه دوست فوق العادم بفرستم تا بگم که خیلی دوست دارم دوست جونم.
امیدوارم خوشحال شده باشی. ناقابل بود.
دوست اول و آخر تو 
${customerName}`,
	TemplateFour: `${userName} جان سلام
شدیدا دلم برات تنگ شده ولی نتونستم بیام ببینمت. یه هدیه کوچیک بود که امیدوارم خوشحال شده باشی. 
همیشه لبت خندون باشه و دلت شاد و تنت سلامت
دوستدار تو 
${customerName}`,
	TemplateFive: `${userName} سلاااااااااااااامممممممممممم
چطورییییییییییی؟!؟!؟؟ خیلی دلم برات تنگ شده بود می خواستم یه هدیه خیلی خاص برات بفرستم تا بدونی که چقد برای من خاص هستیییییی. دلتنگتم شدیییییددددد. از اونجایی که من خیلی خفنم مطمئنم از هدیه ای که برات فرستادم خیلی خوشحال شدی الان :دی 
ببینمت رفیییییقققققق
${customerName}`,
	TemplateSix: `همسر عزیززززممممم سلاممممم عشقققققممممممم
می دونی که چقددددد عاشقتممممم و دیوونتممممم. می خواستم یه کادوی خاص و جدید بهت بدممممم که تا حالا هیچکی بهت ندادهههههه. امیدوارم دوست داشته باشی و حتما باید دوست داشته باشییییی :))))
خیلی خیلی خیلی دوستتتت دارم همسر جااااااااااااان.
دوسسسسسسسسست دارم سبد سبد
باااااااااازززز گل ععععشششششقققق جووونه زززززززدد
دوسسسسسسست دارم یه عالمههههههه
هههررررررر چییییی بگمممممممم بازم کممههههههههه
عشق اول و آخرت
${customerName}`,
	TemplateSeven: `سلام ${userName} عزیزم
می خواستم یه کادوی جدید و خاص به خاص ترین آدم دنیام بدم. خیلی دوست دارم و امیدوارم تونسته باشه مهربون ترین همسر دنیا رو یه کوچولو خوشحال کرده باشم.
عاشقانه عاشقتم عزیزم.
${customerName}`,
	TemplateEight: `${userName} عزیز سلام
دوست داشتم برای تولدت خاص ترین کادو رو بفرستم. واسه همین تصمیم گرفتم
MysteryBox رو انتخاب کنم.
امیدوارم تونسته باشم متفاوت ترین کادویی که تاحالا واسه تولدت گرفته بودی رو بهت هدیه داده باشم.
بهترین ها رو برات آرزو می کنم. امیدوار همیشه شاد و سلامت و پیروز باشی
به یادت هستم
${customerName}`,
	TemplateNine: `${userName}  سلاممممممممممممممم
توللللدددددددد توللللللللللللللللد تولللللللللللدت مباررررررررررررررررک
مباررررررررررک مباررررررررررررررررک تولدت مبارررررررررررررکککک
لبت شااااااااااااد و دلت خوووووووووووووش چو گل پرخنده باشییییییییییییی
بیا ششششمعارو فوت کننننننن که صد سال زنده باشییییییییییییییی
دیری دیرین دیری دیرین
دیریری دیریری دیری دیرین
عاشقتممممممم
از طرف ${customerName}`,
	TemplateTen: `سلام ${userName} عزیز
در این سالروز زمینی شدنت بهترین دعاها را برایت می کنم و آرزوی سلامتی، تندرستی و موفقیت برایت دارم.
همیشه سربلند و پایدار باشی
دوستدار تو
${customerName}`,
	TemplateFree: ''
};
	$('textarea#GiftText').val(giftTexts[selectedGiftText]);
	M.textareaAutoResize($('textarea#GiftText'));
}
	$('#ScrollToDownButton').click(function() {
	if (this.hash !== "") {
		event.preventDefault();
		var hash = this.hash;
		$('html, body').animate({
			scrollTop: $(hash).offset().top
		}, 1000, function(){
			window.location.hash = hash;
		});
		document.getElementById('customerName').focus();
	}
});
//***************************************************************************
//***************************** Calculate Order Price **************************
var discountInfo = {
	giftsPrice: '1000 تومن',
	deliveryPrice: '50 تومن',
	mystreyBoxFee: '2000 تومن',
	totalPrice : '1500 تومن'
};
var selectedGiftItems = [];
$('input[data-type="OptionalGift"').change(function() {
	if ($(this).prop('checked')){
		selectedGiftItems.push($(this).attr('data-price'));
	} else {
		var selectedItem = $(this).attr('data-price');
		selectedGiftItems = selectedGiftItems.filter(function(item) {
			return selectedItem != item;
		});
	}
	setDiscountContnet();
});
function updateDiscountContent(){
	$("#DiscountGifts").html(discountInfo.giftsPrice);
	$("#DiscountFee").html(discountInfo.mystreyBoxFee);
	$("#DiscountDelivery").html(discountInfo.deliveryPrice);
	$("#DiscountTotalPrice").html(discountInfo.totalPrice);
}
function setDiscountContnet() {
	var totalGiftPrice = 0;
	if (selectedGiftItems.length) {
		for (var i = selectedGiftItems.length ; i > 0 ; i--) {
			totalGiftPrice += Number(selectedGiftItems[i - 1].split('_')[0]);
		}
	}
	if (totalGiftPrice > 0) {
		discountInfo.giftsPrice = totalGiftPrice + ' تومان';
		discountInfo.deliveryPrice =' 20000 تومان';
		discountInfo.mystreyBoxFee = ' 10000 تومان';
		discountInfo.totalPrice =(10000 + 20000 + totalGiftPrice) + ' تومان';
	} else {
		discountInfo.giftsPrice = 'رایگان';
		discountInfo.deliveryPrice = 'رایگان';
		discountInfo.mystreyBoxFee =  'رایگان';
		discountInfo.totalPrice =  'رایگان';
	}
	updateDiscountContent();
}
});

$(document).ready(function() {
	var alerts = $("#Mahdi").children();
	for (var i = alerts.length -1 ; i >=0 ; i--) {
		M.toast({html: alerts[i].innerHTML});
	}
});


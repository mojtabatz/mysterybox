$(document).ready(function() {
	var userName = $('#XGameSenderName').html();
	var userNameCounter = 1;
	while(userNameCounter <= userName.length) {
		$('#XGameAnswerWrap').prepend('<span></span>');
		userNameCounter++;
	}
	var letterCounter = 0;
	var userAnswer = ''; 
	$('.X-game .letters span').click(function() {
		if (letterCounter <= userName.length -1) {
			$('#XGameAnswerWrap').children()[letterCounter].innerHTML = $(this).attr('data-letter');
			userAnswer = userAnswer + $(this).attr('data-letter');
			if ($(this).attr('data-letter') == userName.substr(letterCounter, 1)) {
				$('#XGameAnswerWrap').children()[letterCounter].className = 'valid';
			} else {
				$('#XGameAnswerWrap').children()[letterCounter].className = 'invalid';
			}
			if (letterCounter == userName.length - 1 && userName == userAnswer) {
				$('#XGameWrapper').attr('class', 'displayNone');
				$('#XGameWinnigMessage').attr('class', 'big-font cyan-text text-accent-3 center-align winnigMessage');
			}
			letterCounter++;
		}
	});
	$('.X-game .remove-form').click(function() {
		var i = 0;
		while( i < $('#XGameAnswerWrap').children().length) {
			$('#XGameAnswerWrap').children()[i].innerHTML = '';
			$('#XGameAnswerWrap').children()[i].className = '';
			i++;
		}
		userAnswer = '';
		letterCounter = 0;
	});
	
})
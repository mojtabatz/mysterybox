$(document).ready(function(){
	var letters = $('#Letters').children();
	var currentPosition = 0;
	$('#LetterActivator').click(function(){
		if($(letters[currentPosition]).attr('class') == 'char active') {
			$(letters[currentPosition]).attr('class', 'char');
		} else {
			$(letters[currentPosition]).attr('class', 'char active');
		}
		gameFinishedCheker(letters);
	});
	$('#CoursorMover').click(function(){
		if (currentPosition < 0 || currentPosition >= letters.length - 1) {
			currentPosition = 0;
		} else {
			currentPosition++;
		}
	});
});
function gameFinishedCheker(letters){
	var i=0;
	var finished = true;
	while(i < letters.length) {
		if (letters[i].className == 'char') {
			finished = false;
		}
		i++;
	}
	if (finished) {
		$('#LetterActivator').attr('disabled', 'disabled');
		$('#CoursorMover').attr('disabled', 'disabled');
		$('#NextLevelButton').attr('class', 'btn btn-large big-font cyan accent-3 grey-text text-darken-4');
	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['sender_name','sender_last_name','receiver_name','receiver_last_name',
        'receiver_mobile_number','sender_mobile_number','receiver_address','custom_gift','gifts',
        'final_text','pic_path','gift_time','music_path','receiver_user_id'];

    public function user()
    {
        return $this->belongsTo(User::class,'receiver_user_id');
    }
}
